import aiohttp
from aiohttp import web
import sys, asyncio
from asyncio import create_subprocess_shell
from asyncio.subprocess import DEVNULL, STDOUT, PIPE


async def socket_reader (ws):
    async for msg in ws:
        print(msg)
        # if msg.data == 'close':
        #     await ws.close()
        if msg.type == aiohttp.WSMsgType.TEXT:
            print(msg.data)
            cmd = msg.data
            process = await create_subprocess_shell(cmd, stdin = DEVNULL, stdout=PIPE, stderr=STDOUT)
            async for line in process.stdout:
                # print ("gotline", line)
                await ws.send_str(line.decode("utf-8"))

async def websocket_handler(request):
    print('Websocket connection starting')
    ws = web.WebSocketResponse()
    await ws.prepare(request)
    print('Websocket connection ready')
    await socket_reader(ws)
    print('Websocket connection closed')
    return ws

def main ():
    import argparse
    ap = argparse.ArgumentParser("wwwsh")
    args = ap.parse_args()
    if sys.platform == 'win32':
        # Folowing: https://docs.python.org/3/library/asyncio-subprocess.html
        # On Windows, the default event loop is SelectorEventLoop which does not support subprocesses.
        # ProactorEventLoop should be used instead.
        loop = asyncio.ProactorEventLoop()
        asyncio.set_event_loop(loop)

    app = web.Application()
    app.router.add_route('GET', '/', lambda request: web.FileResponse('./socialshell.html'))
    app.router.add_route('GET', '/socialshell.js', lambda request: web.FileResponse('./socialshell.js'))
    app.router.add_route('GET', '/ws', websocket_handler)
    web.run_app(app)

if __name__ == "__main__":
    main()