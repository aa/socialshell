(function () {
var stdin,
  stdout,
  sock;

window.addEventListener("DOMContentLoaded", function () {
  console.log("loaded");
  stdin = document.getElementById("stdin");
  stdout = document.getElementById("stdout");

  try { sock = new WebSocket('ws://' + window.location.host + '/ws'); }
  catch(err) { sock = new WebSocket('wss://' + window.location.host + '/ws'); }
  sock.onopen = function() {
    console.log("sock.onopen");
  };
  sock.onmessage = function(event) {
    // showMessage(event.data);
    var msg = document.createElement("div");
    msg.textContent = event.data;
    stdout.appendChild(msg);
  };
  sock.onclose = function(event){
    if(event.wasClean){
      console.log('Clean connection end')
    } else {
      console.log('Connection broken')
    }
  };
  sock.onerror = function(error){
    console.log("error", error);
  };

  function enter () {
    var val = stdin.value;
    console.log("enter", val);
    sock.send(val);
  }

  stdin.addEventListener("keydown", function (evt) {
     if (evt.keyCode == 13) {
        enter();
     } else if (evt.keyCode == 9) {
        // tab
        return false;
     } else if (evt.charCode == 32) {
        man();
     } else if (evt.keyCode == 8) {
        // Backspace
     } else if (evt.keyCode == 38) {
        // UP
     } else if (evt.keyCode == 40) {
        // DOWN
     } else if (evt.keyCode == 37) {
        // LEFT
     } else if (evt.keyCode == 39) {
        // RIGHT
     } else if (evt.charCode == 92) {
        // PIPE !
     }

  })

});


})();
